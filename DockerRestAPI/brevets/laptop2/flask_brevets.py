"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
from io import StringIO
import csv
from pymongo import MongoClient
import pprint
import flask
from flask import Flask, redirect, url_for, request, render_template, Response, make_response
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from flask_restful import Resource, Api
import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient("mongodb+srv://ntitzler:77Kinder!@cluster0-iazsi.mongodb.net/test?retryWrites=true&w=majority")
db = client.tododb

#logging.basicConfig(format='%(levelname)s:%(message)2',  level=logging.INFO) 
#log = logging.getLogger(__name__)
app.logger.setLevel(logging.DEBUG)

###
# Pages
###

api = Api(app)


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404




###############
#
# API handlers
#
###############
class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
        'Yet another laptop!',
        'Yet yet another laptop!'
            ]
        }

@app.route('/listAll', methods=['GET'])
@app.route('/listAll/json', methods=['GET'])
def get_tasks():
    top = request.args.get('top')
    topInt = int(top)
    numEntries = 0
    _items = db.tododb.find()
    items = [item for item in _items]
    itemStr = ""
    for item in items:
        if (numEntries == topInt):
            break;
        else:
            itemStr += "("+item['open_time'] + ", "+ item['close_time'] + ")" + ", "
            numEntries+=1
    
    return flask.jsonify(openAndCloseTime=itemStr)


@app.route('/listOpenOnly', methods=['GET'])
@app.route('/listOpenOnly/json', methods=['GET'])
def list_open():
    top = request.args.get('top')
    topInt = int(top)
    numEntries = 0
    _items = db.tododb.find()
    items = [item for item in _items]
    itemStr = ""
    for item in items:
        if (numEntries == topInt):
            break;
        else:
            itemStr += item['open_time'] + ", "
            numEntries+=1

    
    return flask.jsonify(OpenTimes=itemStr)

@app.route('/listCloseOnly', methods=['GET'])
@app.route('/listCloseOnly/json', methods=['GET'])
def list_close():
    
    _items = db.tododb.find()
    items = [item for item in _items]
    itemStr = ""
    top = request.args.get('top')
    topInt = int(top)
    numEntries = 0
    for item in items:
        if (numEntries == topInt):
            break;
        else:
            itemStr += item['close_time'] + ", "
            numEntries+=1

    
    return flask.jsonify(CloseTimes=itemStr)

@app.route('/listAll/csv', methods=['GET'])
def list_all_csv():
    top = request.args.get('top')

    app.logger.debug("top listAll={}".format(top))

    _items = db.tododb.find()
    items = [item for item in _items]
    closeTimes = []
    topInt = int(top)
    numEntries = 0
    app.logger.debug("top int={}".format(top))
    for item in items:
        if (numEntries == topInt):
            break;
        else:
            closeTimes.append({item['open_time'],item['close_time']})
            numEntries+=1
        

    
    si = StringIO()
    cw = csv.writer(si)
    cw.writerow(closeTimes)
    output = make_response(si.getvalue())
    output.headers["Content-Dispostion"] = "attachment; filename=CloseOnly.csv"
    output.headers["Content-type"] = "text.csv"
    return output

@app.route('/listOpenOnly/csv', methods=['GET'])
def list_open_csv():
    top = request.args.get('top')
    topInt = int(top)
    numEntries = 0
    _items = db.tododb.find()
    items = [item for item in _items]
    openTimes = []
    for item in items:
        if (numEntries == topInt):
            break;
        else:
            openTimes.append(item['open_time'])
            numEntries+=1

    
    si = StringIO()
    cw = csv.writer(si)
    cw.writerow(openTimes)
    output = make_response(si.getvalue())
    output.headers["Content-Dispostion"] = "attachment; filename=OpenOnly.csv"
    output.headers["Content-type"] = "text.csv"
    return output

@app.route('/listCloseOnly/csv', methods=['GET'])
def list_close_csv():
    top = request.args.get('top')
    topInt = int(top)
    numEntries = 0
    _items = db.tododb.find()
    items = [item for item in _items]
    closeTimes = []
    app.logger.debug("top int={}".format(top))
    for item in items:
        if (numEntries == topInt):
            break;
        else:
            closeTimes.append(item['close_time'])
            numEntries+=1

    
    si = StringIO()
    cw = csv.writer(si)
    cw.writerow(closeTimes)
    output = make_response(si.getvalue())
    output.headers["Content-Dispostion"] = "attachment; filename=CloseOnly.csv"
    output.headers["Content-type"] = "text.csv"
    return output
###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############




@app.route('/db')
def new():
    #parse input string
    timesString = request.args.get('timesString');
    timeArray = timesString.split("x");
    app.logger.debug("timeArray={}".format(timeArray))

    #clear database
    db.tododb.drop()


    half = (len(timeArray)-1)
    app.logger.debug("half={}".format(half))
    for i in range(0, len(timeArray)-1, 2):
        #app.logger.debug("timesArray[i]={}".format(timeArray[i]));
        #app.logger.debug("timesArray[i+1]={}".format(timeArray[i+1]));
        item_doc = {'open_time': timeArray[i], 'close_time':timeArray[i+1]}
        db.tododb.insert_one(item_doc);


    
    for obj in db.tododb.find():
        print(obj['open_time'])
        #app.logger.debug("open_time={}, close_time={}".format(obj['open_time'], obj['close_time']))
    

    

    result = {"success": "success"}
    return flask.jsonify(result=result)



@app.route("/_display")
def dis():

    _items = db.tododb.find()
    items = [item for item in _items]

    if (len(items) == 0):
        
        return render_template('subError.html');


    for item in items:
        app.logger.debug("open_time11={}, close_time={}".format(item['open_time'], item['close_time']))

    return render_template('todo.html', items=items)




@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    trueDistance = request.args.get('trueDistance',type=int)
    app.logger.debug("trueDistance={}".format(trueDistance))

    startTime = request.args.get('startTime',type=str)
    date = request.args.get('date',type=str)
    app.logger.debug("startTime={}".format(startTime))
    app.logger.debug("date={}".format(date))

     #"2013-05-05T12:30:45+00:00"
    inputStr = date + "T" + startTime + "+" + "00:00"
    #print("inputStr: ",inputStr)
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    

    # I changed the code here to shift the time by 8 hours because without it 
    # moment.js would misformat the string. This bug was encountered on line 149,150 in calc.html
    open_time = acp_times.open_time(km, trueDistance, inputStr)

    if (open_time == 0):
        error = "error"
        result = {"error": error}
        return flask.jsonify(result = result)
    
    dt = open_time.split("T")

    date = dt[0].split("-")
    zone = dt[1].split("+")[1]
    time = dt[1].split("+")[0]
    time = time.split(":")

    start_time2 = arrow.Arrow(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
    start_time2 = start_time2.shift(hours = +8)
    open_time = start_time2.isoformat()

    close_time = acp_times.close_time(km, trueDistance, inputStr)
    dt = close_time.split("T")
    date = dt[0].split("-")
    zone = dt[1].split("+")[1]
    time = dt[1].split("+")[0]
    time = time.split(":")

    close_time2 = arrow.Arrow(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
    close_time2 = close_time2.shift(hours = +8)
    close_time = close_time2.isoformat()


    result = {"open": open_time, "close": close_time}
    
    return flask.jsonify(result=result)


#############
api.add_resource(Laptop, '/')

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
