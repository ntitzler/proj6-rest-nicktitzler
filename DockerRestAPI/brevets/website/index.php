<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <h1>List of services</h1>
        <ul>
            <?php
            $json = file_get_contents('http://web/');
            $obj = json_decode($json);
              $laptops = $obj->Laptops;
            foreach ($laptops as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>

        <h1>List of laptops</h1>
        <ul>
            <?php
            //$json = file_get_contents('http://0.0.0.0:5000/');
            $obj = json_decode($json);
	          $listAll = $obj->ListAll;
            foreach ($listAll as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
        <h1>List of pizza</h1>
        <ul>
            <?php
            //$json = file_get_contents('http://0.0.0.0:5000/');
            $obj = json_decode($json);
              $pizza = $obj->Pizza;
            foreach ($pizza as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
    </body>
</html>
